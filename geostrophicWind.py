import numpy as np
import matplotlib.pyplot as plt

pa=1e5
pb=200
f=1e-4
rho=1.
L=2.4e6
ymin=0.
ymax=1e6

N=10
dy=(ymax-ymin)/N

y=np.linspace(ymin,ymax,N+1)
p=pa+pb*np.cos(y*np.pi/L)

uExact=pb*np.pi*np.sin(y*np.pi/L)/(rho*f*L)

uNum=np.zeros_like(y)
uNum[0]=-1/(rho*f)*(p[1]-p[0])/dy
uNum[N]=-1/(rho*f)*(p[N]-p[N-1])/dy
for i in range(1,N):
    uNum[i]=-1/(rho*f)*(p[i+1]-p[i-1])/(2*dy)

plt.plot(y/1000, uNum,'k--',label='Numerically u',color='b',markeredgewidth=1.5)
plt.plot(y/1000, uExact,'k--',label='Exact u',color='r',markeredgewidth=1.5)
plt.legend(loc='best')
plt.xlabel('y(km)')
plt.ylabel('u(m/s)')
plt.tight_layout()
plt.savefig('geoWindCent.pdf')
plt.show()

plt.plot(y/1000,uNum-uExact,'k--',markeredgewidth=1.5)
plt.legend(loc='best')
plt.xlabel('y(km)')
plt.ylabel('u error(m/s)')
plt.tight_layout()
plt.savefig('geoWindErrorsCent.pdf')
plt.show()